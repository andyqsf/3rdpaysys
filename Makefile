# 避免資料夾已經存在問題, 才能執行跟資料夾同名的cmd
.PHONY: proto truss postman
PKG_SERVICE ?= goapp
MAKEFLAGS += -j5

all: demo

copy.config:
	cp configs/app-dev.yml configs/app.yml

demo:
	go run cmd/$(PKG_SERVICE)/main.go demo

demo_linux:
	GOAPP_HOME=$(CURDIR) go run cmd/$(PKG_SERVICE)/*.go demo

build:
	go build ./cmd/goapp

stats:
	go run cmd/$(PKG_SERVICE)/main.go stats

api:
	go run cmd/$(PKG_SERVICE)/main.go api

jobs:
	go run cmd/$(PKG_SERVICE)/main.go jobs

tidy:
	go mod tidy
