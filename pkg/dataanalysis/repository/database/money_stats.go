package database

import (
	"goapp/internal/pkg/db"
	"goapp/pkg/dataanalysis"

	log "github.com/sirupsen/logrus"

	"gorm.io/gorm"
)

func NewMoneyStatsRepository(writeDB *gorm.DB) dataanalysis.MoneyStatsRepository {
	return &MoneyStatsRepo{
		writeDB,
	}
}

type MoneyStatsRepo struct {
	writeDB *gorm.DB
}

func (repo *MoneyStatsRepo) Create(model *dataanalysis.MoneyStats) error {
	return repo.writeDB.Create(model).Error
}

func (repo *MoneyStatsRepo) CreateList(list []dataanalysis.MoneyStats) error {
	if len(list) == 0 {
		return nil
	}
	return repo.writeDB.Create(&list).Error
	//return repo.writeDB.CreateInBatches(list, 100).Error
}

func (repo *MoneyStatsRepo) TypeList(notIn []int32) ([]dataanalysis.MoneyStatsType, error) {
	var result []dataanalysis.MoneyStatsType
	err := repo.writeDB.Model(&dataanalysis.MoneyStatsType{}).
		Not(notIn).
		Find(&result).Error
	return result, err
}

func (repo *MoneyStatsRepo) DeleteList(statsTime db.Timestamp) {
	result := repo.writeDB.Where("stats_time = ?", statsTime).Delete(dataanalysis.MoneyStats{})
	if result.RowsAffected == 0 {
		log.Info("target deleted money_stats not found, stats_time=%v", statsTime.Time)
	}
	if result.Error != nil {
		log.Panicf("delete money_stats stats_time=%v error: %v", statsTime, result.Error)
	}
}

func (repo *MoneyStatsRepo) QueryList(start, end db.Timestamp) ([]dataanalysis.MoneyStats, error) {
	var result []dataanalysis.MoneyStats
	err := repo.writeDB.Model(&dataanalysis.MoneyStats{}).
		Where("stats_time between ? AND ?", start, end).
		Find(&result).Error
	return result, err
}
