package http

import (
	"github.com/labstack/echo/v4"
	echoMw "github.com/labstack/echo/v4/middleware"
)

func SetRoutes(engine *echo.Echo, handler *Handler) {
	rg := engine.Group("/api")
	rg.GET("/query", handler.queryMoneyStatsEndpoint)
	rg.PUT("/import", handler.importMoneyStatsEndpoint)

	engine.Use(echoMw.StaticWithConfig(echoMw.StaticConfig{
		Root:   "web",        // This is the path to your SPA build folder, the folder that is created from running "npm build"
		Index:  "index.html", // This is the default html page for your SPA
		Browse: false,
		HTML5:  true,
	}))

}
