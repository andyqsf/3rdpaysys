package database

import (
	"goapp/pkg/stats"
	"time"

	"gorm.io/gorm"
)

func NewTestTable1Repository(readDB, writeDB *gorm.DB) stats.TestTable1Repository {
	return &TestTable1Repo{
		readDB,
		writeDB,
	}
}

type TestTable1Repo struct {
	readDB  *gorm.DB
	writeDB *gorm.DB
}

func (repo *TestTable1Repo) WriteDB() *gorm.DB {
	return repo.writeDB
}

func (repo *TestTable1Repo) GetSumData(start, end time.Time) (*stats.CustomTestTable1, error) {
	var result stats.CustomTestTable1
	err := repo.readDB.Table((&stats.TestTable1{}).TableName()).
		Select("sum(money1) as total1, sum(money2) as total2").
		Where("date_time BETWEEN ? AND ?", start, end).
		First(&result).Error
	return &result, err
}

func (repo *TestTable1Repo) ListByTime(start, end time.Time) ([]stats.TestTable1, error) {
	var result []stats.TestTable1
	err := repo.readDB.Model(&stats.TestTable1{}).
		Where("date_time BETWEEN ? AND ?", start, end).
		Find(&result).Error
	return result, err
}

func (repo *TestTable1Repo) JoinAnotherDbTable(start, end time.Time) ([]stats.TestTable1, error) {
	//err := repo.readDB.Table("testdb1.test_table1").
	//	Joins("inner join testdb2.test_table2 t on money1 = t.total1").
	//	Where("test_table1.date_time BETWEEN ? AND ?", start, end).
	//	Find(&result).Error

	//var result []stats.TestTable1
	//err := repo.readDB.Table("testdb1.test_table1 a").
	//	Select("a.money1 as money1, ? as money2", 60).
	//	Joins("inner join testdb2.test_table2 b on a.money1 = b.total1").
	//	Where("a.date_time BETWEEN ? AND ?", start, end).
	//	Find(&result).Error

	var result []stats.TestTable1
	session := repo.readDB.Table("testdb1.test_table1 a").
		Select("a.money1 as money1, ? as money2", 60).
		Joins("inner join testdb2.test_table2 b on a.money1 = b.total1").
		Where("a.date_time BETWEEN ? AND ?", start, end)

	session = session.Where("a.money1 > 300")
	err := session.Find(&result).Error

	return result, err
}
