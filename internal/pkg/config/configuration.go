package config

import (
	"flag"
	"io/ioutil"
	stdlog "log"
	"os"
	"path/filepath"
	"sync"

	log "github.com/sirupsen/logrus"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

// TODO: 建議搭配cobra引入viper, 檢查傳入的config初始化配置是否正確沒有遺漏

var (
	// EnvPrefix 環境變數的前墬
	EnvPrefix           string
	globalConfiguration *GlobalConfiguration
	once                sync.Once
)

// GlobalConfiguration 程式內取用的系統設定參數 只暴露程式內需要用到的參數
type GlobalConfiguration struct {
	config Configuration
}

// LogSetting 用來設定 log 相關資訊
type LogSetting struct {
	Name             string `yaml:"name"`
	Type             string `yaml:"type"`
	ConnectionString string `yaml:"connection_string"`
	MinLevel         string `yaml:"min_level"`
}

// DatabaseCfg Database的設定檔，用來提供連線的資料庫數據
type DatabaseCfg struct {
	Name     string
	Address  string
	Username string
	Password string
	DBName   string
}

// GetDatabaseCfg 取得目前的DatabaseCfg配置
func GetDatabaseCfg() []DatabaseCfg {
	return globalConfiguration.config.DatabasesCfg
}

// RedisCfg Redis的設定檔
type RedisCfg struct {
	ClusterMode     bool     `yaml:"cluster_mode"`
	Addresses       []string `yaml:"addresses"`
	Password        string   `yaml:"password"`
	MaxRetries      int      `yaml:"max_retries"`
	PoolSizePerNode int      `yaml:"pool_size_per_node"`
	DB              int      `yaml:"db"`
}

// GetRedisCfg 取得目前的RedisCfg配置
func GetRedisCfg() RedisCfg {
	return globalConfiguration.config.RedisCfg
}

type SchedulerCfg struct {
	MoneyStatsFreq string `yaml:"money_stats_freq"`
}

func GetSchedulerCfg() SchedulerCfg {
	return globalConfiguration.config.SchedulerCfg
}

// IdentityCfg Identity的設定檔
type IdentityCfg struct {
	AdvertiseAddr string `yaml:"advertise_addr"`
	GRPCBind      string `yaml:"grpc_bind"`
	HTTPBind      string `yaml:"http_bind"`
}

// GetIdentityCfg 取得目前的IdentityCfg配置
func GetIdentityCfg() IdentityCfg {
	return globalConfiguration.config.IdentityCfg
}

type APIServerCfg struct {
	AdvertiseAddr string `yaml:"advertise_addr"`
	HTTPBind      string `yaml:"http_bind"`
}

func GetAPIServerCfg() APIServerCfg {
	return globalConfiguration.config.APIServerCfg
}

// Secrets 資料庫加密設定
type Secrets struct {
	Database string `yaml:"database"`
}

// Configuration 用來代表 config 設定物件
type Configuration struct {
	Env          string        `yaml:"env"`
	Mode         string        `yaml:"mode"`
	DatabasesCfg []DatabaseCfg `yaml:"databases"`
	RedisCfg     RedisCfg      `yaml:"redis"`
	LogsCfg      []LogSetting  `yaml:"logs"`
	SchedulerCfg SchedulerCfg  `yaml:"scheduler"`
	IdentityCfg  IdentityCfg   `yaml:"identity"`
	APIServerCfg APIServerCfg  `yaml:"apiserver"`
	Secrets      Secrets       `yaml:"secrets"`
}

// GetEnv 取得目前配置的環境設定
func GetEnv() string {
	return globalConfiguration.config.Env
}

// New function 創建一個 configuration instance 出來
func New(fileName string) *Configuration {
	flag.Parse()
	c := Configuration{}

	// TODO: 如果可以，拿掉envionment variables
	rootDirPath := os.Getenv("GOAPP_HOME")
	if rootDirPath == "" {
		basePath, err := os.Getwd()
		if err != nil {
			log.Println(err)
		}
		rootDirPath = basePath
	}

	configPath := filepath.Join(rootDirPath, "configs", fileName)
	_, err := os.Stat(configPath)
	if err != nil {
		stdlog.Fatalf("config: file error: %s", err.Error())
	}

	// config exists
	file, err := ioutil.ReadFile(filepath.Clean(configPath))
	if err != nil {
		stdlog.Fatalf("config: read file error: %s", err.Error())
	}

	err = yaml.Unmarshal(file, &c)
	if err != nil {
		stdlog.Fatal("config: yaml unmarshal error:", err)
	}

	if EnvPrefix == "" {
		stdlog.Fatal("config: env prefix not set")
	}

	if err := envconfig.Process(EnvPrefix, &c); err != nil {
		stdlog.Fatal("config: env failed:", err)
	}

	if c.Mode == "" {
		c.Mode = "debug"
	}

	return &c
}

// SetGlobalConfiguration 設定程式內取用的系統設定參數
func SetGlobalConfiguration(cfg *Configuration) {
	once.Do(func() {
		globalConfiguration = &GlobalConfiguration{*cfg}
	})
	return
}
