package db

import "goapp/internal/pkg/json2"

// ToMap convert bean to map[string]interface{}
func ToMap(bean interface{}) map[string]interface{} {
	if bean == nil {
		return map[string]interface{}{}
	}
	if ret, ok := bean.(map[string]interface{}); ok {
		return ret
	}
	ret := map[string]interface{}{}
	b, _ := json2.Marshal(bean)
	_ = json2.Unmarshal(b, &ret)
	return ret
}
