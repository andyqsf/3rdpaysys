package db

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/cenk/backoff"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"goapp/internal/pkg/config"

	log "github.com/sirupsen/logrus"
)

// InitDatabases init and return write and read DB objects
func InitDatabases(databasesCfg []config.DatabaseCfg, name string) (*gorm.DB, *gorm.DB, error) {
	var err error
	// setup database
	var readDB, writeDB *gorm.DB
	for _, database := range databasesCfg {
		if strings.EqualFold(database.Name, name+"_write") {
			writeDB, err = setupDatabase(database)
			if err != nil {
				return nil, nil, err
			}
		} else if strings.EqualFold(database.Name, name+"_read") {
			readDB, err = setupDatabase(database)
			if err != nil {
				return nil, nil, err
			}
		}
	}
	var sqlDB *sql.DB
	if readDB == nil {
		return nil, nil, fmt.Errorf("read db intialization was failed, name: %s", name)
	} else {
		sqlDB, err = readDB.DB()
		if err != nil {
			return nil, nil, err
		}
		if err = sqlDB.Ping(); err != nil {
			return nil, nil, fmt.Errorf("read db ping was failed")
		}
	}
	if writeDB == nil {
		return nil, nil, fmt.Errorf("write db intialization was failed, name: %s", name)
	} else {
		sqlDB, err = readDB.DB()
		if err != nil {
			return nil, nil, err
		}
		if err = sqlDB.Ping(); err != nil {
			return nil, nil, fmt.Errorf("write db ping was failed")
		}
	}

	return readDB, writeDB, nil
}

const driverName = "ocsql"

func init() {
	//driver := ocsql.Wrap(
	//	mysql.MySQLDriver{},
	//	ocsql.WithAllTraceOptions(),
	//	ocsql.WithRowsClose(false),
	//	ocsql.WithRowsNext(false),
	//	ocsql.WithDisableErrSkip(true),
	//)
	//sql.Register(driverName, driver)
	//ocsql.RegisterAllViews()
}

func setupDatabase(database config.DatabaseCfg) (*gorm.DB, error) {
	bo := backoff.NewExponentialBackOff()
	bo.MaxElapsedTime = time.Duration(180) * time.Second
	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=true&multiStatements=true", database.Username, database.Password, database.Address, database.DBName)

	//log.Debugf("main: database connection string: %s", connectionString)
	//var driver *sql.DB
	var err error
	//driver, err = sql.Open(driverName, connectionString)
	//if err != nil {
	//	log.Panicf("sql open error: %s", err)
	//}

	var db *gorm.DB
	var sqlDB *sql.DB
	err = backoff.Retry(func() error {
		db, err = gorm.Open(mysql.New(mysql.Config{
			DSN:                       connectionString,
			DefaultStringSize:         256,   // add default size for string fields, by default, will use db type `longtext` for fields without size, not a primary key, no index defined and don't have default values
			DisableDatetimePrecision:  true,  // disable datetime precision support, which not supported before MySQL 5.6
			DontSupportRenameIndex:    true,  // drop & create index when rename index, rename index not supported before MySQL 5.7, MariaDB
			DontSupportRenameColumn:   true,  // use change when rename column, rename rename not supported before MySQL 8, MariaDB
			SkipInitializeWithVersion: false, // smart configure based on used version
		}), &gorm.Config{
			NowFunc: NowFunc,
		})
		//db, err = gorm.Open("mysql", driver)
		if err != nil {
			log.Errorf("main: mysql open failed: %v", err)
			return err
		}
		sqlDB, err = db.DB()
		if err != nil {
			return err
		}
		err = sqlDB.Ping()
		if err != nil {
			log.Errorf("main: mysql ping error: %v", err)
			return err
		}
		return nil
	}, bo)

	if err != nil {
		log.Panicf("main: mysql connect err: %s", err.Error())
	}

	//db.LogMode(true)
	//db.Debug()

	log.Infof("database ping success")
	sqlDB.SetMaxIdleConns(150)
	sqlDB.SetMaxOpenConns(300)
	sqlDB.SetConnMaxLifetime(14400 * time.Second)

	return db, nil
}

var NowFunc = func() time.Time {
	return time.Now()
}
