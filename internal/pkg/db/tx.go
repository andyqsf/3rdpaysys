package db

import (
	"context"

	"gorm.io/gorm"
)

// ExecuteTx 執行一個 Database 交易，如果 `fn` 執行過程中遇到失敗，會自動執行 rollback, 如果成功則會自動 commit,
// 另外需要注意的地方是 db 的 connection 需要用 write 的 connection 來執行 tx
func ExecuteTx(ctx context.Context, db *gorm.DB, fn func(*gorm.DB) error) error {
	// Start a transactions.
	tx := db.Begin()
	return executeInTx(ctx, tx, func() error { return fn(tx) })
}

func executeInTx(ctx context.Context, tx *gorm.DB, fn func() error) (err error) {
	defer func() {
		if r := recover(); r != nil {
			_ = tx.Rollback()
			panic(r)
		}
		if err == nil {
			_ = tx.Commit()
		} else {
			_ = tx.Rollback()
		}
	}()
	err = fn()
	return err
}
