package db

import (
	"database/sql/driver"
	"strconv"
	"time"
)

// SystemZeroTimestamp 系統定義的零時
var SystemZeroTimestamp = Timestamp{Time: time.Unix(0, 0).UTC()}

// Timestamp 支援 timestamp 或是 time.Time 格式
type Timestamp struct {
	time.Time
	Valid bool // Valid is true if Time is not NULL
}

// UnmarshalJSON 支援 timestamp 或是 time.Time 格式, 空字串會回 time.Unix(0, 0).UTC()
func (t *Timestamp) UnmarshalJSON(data []byte) error {
	dataStr := string(data)
	if data == nil || dataStr == `""` {
		*t = Timestamp{Time: time.Unix(0, 0).UTC()}
		return nil
	}
	sec, err := strconv.ParseInt(dataStr, 10, 64)
	if err != nil {
		return t.Time.UnmarshalJSON(data)
	}
	*t = Timestamp{Time: time.Unix(sec, 0)}
	return nil
}

// Scan from db
func (t *Timestamp) Scan(src interface{}) error {
	switch ty := src.(type) {
	case string:
		t.Time, _ = time.Parse("2006-01-02T15:04:05.9999Z", ty)
		t.Valid = true
	case time.Time:
		t.Time, t.Valid = src.(time.Time)
	}
	return nil
}

// Value to db
func (t Timestamp) Value() (driver.Value, error) {
	if !t.Time.IsZero() {
		return t.Time, nil
	}
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}
