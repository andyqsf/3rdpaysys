package db_test

import (
	"goapp/internal/pkg/db"
	"os"
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestSQLBuilderOmit(t *testing.T) {
	type Product struct {
		ID        int64        `json2:"id"`
		Code      string       `json2:"code"`
		Name      string       `json2:"name"`
		CreatedAt db.Timestamp `json2:"created_at"`
		UpdatedAt db.Timestamp `json2:"updated_at"`
	}
	dbFile := "test2.db"
	writeDB, err := gorm.Open(sqlite.Open(dbFile), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	defer func() {
		sqlDB, _ := writeDB.DB()
		sqlDB.Close()
	}()
	writeDB.Debug()
	//writeDB.LogMode(true)

	// Migrate the schema
	writeDB.AutoMigrate(&Product{})

	// Create
	builder := db.MergeSQLBuilder(db.Omit("code"))
	session := writeDB.Model(&Product{})
	session = builder.Apply(session, &Product{})
	p := Product{Code: "Create"}
	err = session.Create(&p).Error
	if err != nil {
		t.Error(err)
	}
	if p.ID == 0 {
		t.Error("id must not be 0")
	}
	err = writeDB.Create(&Product{Code: "Create"}).Error
	if err != nil {
		t.Error(err)
	}

	var count int64
	err = writeDB.Model(&Product{}).Where("code = ?", "Create").Count(&count).Error
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("count must be 1")
	}

	// Update Omit
	builder = db.MergeSQLBuilder(db.Omit("id", "updated_at"), db.Where("id = ?", 1))
	session = writeDB.Model(&Product{})
	session = builder.Apply(session, &Product{})
	err = session.Updates(db.ToMap(&Product{Code: "Update"})).Error
	if err != nil {
		t.Error(err)
	}
	err = writeDB.Model(&Product{}).Where("code = ?", "Update").Count(&count).Error
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("Update Omit: count must be 1")
	}

	// Update Only
	builder = db.MergeSQLBuilder(db.Only("name"), db.Where("id = ?", 1))
	session = writeDB.Model(&Product{})
	session = builder.Apply(session, &Product{})
	err = session.Updates(db.ToMap(&Product{Code: "Delete", Name: "Name"})).Error
	if err != nil {
		t.Error(err)
	}
	err = writeDB.Model(&Product{}).Where("name = ?", "Name").Count(&count).Error
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Errorf("Update Only: count must be 1 but get %d", count)
	}

	// Find
	builder = db.MergeSQLBuilder(db.Order("code desc"), db.Order("name asc"), db.Where("code = ?", "Update"))
	session = writeDB.Model(&Product{})
	session = builder.Apply(session, &Product{})
	err = session.Find(&[]Product{}).Error
	if err != nil {
		t.Error(err)
	}

	// Delete
	builder = db.MergeSQLBuilder(db.Omit("code"), db.Where("code = ?", "Update"))
	session = writeDB.Model(&Product{})
	session = builder.Apply(session, &Product{})
	err = session.Delete(&Product{}).Error
	if err != nil {
		t.Error(err)
	}
	err = writeDB.Model(&Product{}).Count(&count).Error
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("count must be 1")
	}
	os.Remove(dbFile)
}
