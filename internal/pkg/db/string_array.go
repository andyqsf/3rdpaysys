package db

import (
	"database/sql/driver"
	"encoding/json"
)

// StringArray is for database and swagger field
type StringArray []string

// Scan from db
func (t *StringArray) Scan(src interface{}) error {
	return json.Unmarshal(src.([]byte), t)
}

// Value to db null will by empty list
func (t StringArray) Value() (driver.Value, error) {
	if t == nil {
		return []byte("[]"), nil
	}
	return json.Marshal(t)
}
