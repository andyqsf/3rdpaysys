package db_test

import (
	"encoding/json"
	"goapp/internal/pkg/db"
	"os"
	"testing"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestTimestamp_UnmarshalJSON(t *testing.T) {
	type fields struct {
		Time db.Timestamp `json:"time"`
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{"int64", fields{}, args{[]byte(`{"time":1535081768}`)}, false},
		{"time.Time", fields{}, args{[]byte(`{"time":"2018-08-24T11:36:08+08:00"}`)}, false},
		{"empty string", fields{}, args{[]byte(`{"time":""}`)}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := json.Unmarshal(tt.args.data, &tt.fields); (err != nil) != tt.wantErr {
				t.Errorf("db.Timestamp.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			b, err := json.Marshal(tt.fields)
			if err != nil {
				t.Errorf("db.Timestamp.MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			t.Logf(string(b))
		})
	}
}

func TestGorm(t *testing.T) {
	type Product struct {
		Code      string
		UpdatedAt db.Timestamp
	}
	writeDB, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	defer func() {
		sqlDB, _ := writeDB.DB()
		sqlDB.Close()
	}()

	// Migrate the schema
	writeDB.AutoMigrate(&Product{})

	// Create
	err = writeDB.Create(&Product{Code: "L1212", UpdatedAt: db.Timestamp{Time: time.Now().UTC()}}).Error
	if err != nil {
		t.Error(err)
	}

	// Read
	var product Product
	// writeDB.First(&product, 1)                   // find product with id 1
	err = writeDB.First(&product, "code = ?", "L1212").Error // find product with code l1212
	if err != nil {
		t.Error(err)
	}

	// Update - update product's price to 2000
	err = writeDB.Model(&product).Update("UpdatedAt", db.Timestamp{Time: time.Now().UTC()}).Error
	if err != nil {
		t.Error(err)
	}

	// Delete - delete product
	err = writeDB.Delete(&product).Error
	if err != nil {
		t.Error(err)
	}
	os.Remove("test.db")
}
