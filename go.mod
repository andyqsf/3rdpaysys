module goapp

go 1.16

require (
	github.com/ahmetb/go-linq/v3 v3.2.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cenk/backoff v2.2.1+incompatible
	github.com/denisenkom/go-mssqldb v0.0.0-20200206145737-bbfc9a55622e // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.10
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.2.1
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/swaggo/echo-swagger v1.1.0
	github.com/swaggo/swag v1.7.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.7
)
