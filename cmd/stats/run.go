package stats

import (
	"goapp/internal/pkg/config"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "stats",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		config.EnvPrefix = "goapp"
		cfg := config.New("app.yml")
		err := initialize(cfg)
		if err != nil {
			log.Panicf("main: initialize failed: %v", err)
			return
		}
		//run()
		run2()
	},
}
