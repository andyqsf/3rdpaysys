package dataanalysis

import (
	"context"
	"fmt"
	"goapp/internal/pkg/config"
	"goapp/internal/pkg/db"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "api",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		defer func() {
			if r := recover(); r != nil {
				// unknown error
				err, ok := r.(error)
				if !ok {
					err = fmt.Errorf("unknown error: %v", r)
				}
				trace := make([]byte, 4096)
				runtime.Stack(trace, true)
				log.WithField("stack_trace", string(trace)).WithError(err).Panic("unknown error")
			}
		}()
		// 配合現行DB, 用utc時區存local時間
		db.NowFunc = func() time.Time {
			format := "2006-01-02 15:04:05"
			timeString := time.Now().Format(format)
			now, _ := time.ParseInLocation(format, timeString, time.UTC)
			return now
		}

		config.EnvPrefix = "goapp"
		cfg := config.New("app.yml")

		// start http server
		httpServer := &http.Server{
			Addr: cfg.APIServerCfg.HTTPBind,
			Handler: func(next http.Handler) http.Handler {
				return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					method := r.Header.Get("X-HTTP-Method-Override")
					if method != "" {
						r.Method = method
					}
					next.ServeHTTP(w, r)
				})
			}(newEchoHandler(cfg)),
		}
		exitCode := 0
		go func() {
			// service connections
			log.Debugf("main: Listening and serving HTTP on %s\n", httpServer.Addr)
			err := httpServer.ListenAndServe()
			if err != nil {
				log.Panicf("main: http server listen failed: %v\n", err)
				exitCode++
			}
		}()

		stopChan := make(chan os.Signal, 1)
		signal.Notify(stopChan, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP, syscall.SIGTERM)
		<-stopChan
		log.Info("main: shutting down server...")

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		if err := httpServer.Shutdown(ctx); err != nil {
			log.Errorf("main: http server shutdown error: %v", err)
			exitCode++
		} else {
			log.Info("main: gracefully stopped")
		}
		os.Exit(exitCode)
	},
}
