package main

import (
	"fmt"
	"goapp/cmd/dataanalysis"
	"goapp/cmd/demo"
	"goapp/cmd/scheduler"
	"goapp/cmd/stats"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "root",
	Short: "choose instance to run: demo, app1, app2, ...",
	Long:  ``,
}

func main() {
	rootCmd.AddCommand(demo.Cmd)
	rootCmd.AddCommand(stats.Cmd)
	rootCmd.AddCommand(dataanalysis.Cmd)
	rootCmd.AddCommand(scheduler.Cmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
